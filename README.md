# pifi-reflector    

Simple script to set up a raspberry pi as a wifi->LAN extender.

Uses [bashconf](https://github.com/marsbard/bashconf) for configuration. Before
running `go.sh` do `git submodule update --init`.

If you specify multiple SSID/PSK pairs then if one connection fails it will 
fall over to the next. To do so, for the `wlan_creds` variable specify the
pairs like `ssid1:psk1 ssid2:psk2 ssid3:psk3`

There are two useful scripts included:

* `scripts/boost` - on certain WLAN devices (RAlink seem to work) this script will put the device into a country code (Guyana) which does not limit the output power of the device and will try to set to 30dB amplification.
* `scripts/re-wpa.sh` - kill any running wpa_supplicant instances and restart in the foreground. Useful for debugging.


Running `go.sh` the first time you will see this:

```
        =================================
        pifi-reflector - Pi Wifi Extender
        =================================

Idx     Param                     Value

[1]     wlan_if
[2]     lan_if
[3]     lan_addr                  172.16.0.1
[4]     lan_dhcp_range            172.16.0.100 172.16.0.199
[5]     wlan_creds

Please choose an index number to edit, I to install, or Q to quit
 ->
```

You will need to set the `wlan_if` parameter (probably "wlan0"), the `lan_if`
parameter (probably "eth0") and the `wlan_creds` settings. The interface 
questions will only show you available interfaces.

You can change the ``lan_addr`` to something else that you like but be sure
to set the ``lan_dhcp_range`` to match.

After configuration it should look more like this:
```
        =================================
        pifi-reflector - Pi Wifi Extender
        =================================

Idx     Param                     Value

[1]     wlan_if                   wlan0
[2]     lan_if                    eth0
[3]     lan_addr                  172.16.0.1
[4]     lan_dhcp_range            172.16.0.100 172.16.0.199
[5]     wlan_creds                HOME_WLAN:easypeasy BUDDY_LAN:lamepass

Please choose an index number to edit, I to install, or Q to quit
 ->

```
Hit `I` to run the installer.

The script sets up an `/etc/rc.local` that attempts to reinitialise the device
to a higher power setting and then sets up the iptables rules for forwarding, by
calling the script `/usr/bin/forwarding-iptables` which the `go.sh` script writes.
Additionally it sets up a systemd service file as well as ensuring that `dnsmasq`
is running to serve addresses on DHCP.