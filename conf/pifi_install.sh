#
# VERY VERY INTERSTING!
# modify wifi power https://askubuntu.com/a/605511
#




if [ "$UID" != "0" ]
then
	echo You must be root to run $0
	exit 1
fi


(
cat <<WPA_SUP
country=ES
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
WPA_SUP
) > /etc/wpa_supplicant/wpa_supplicant.conf


WLAN_CREDS=`get_param wlan_creds`
for pair in $WLAN_CREDS
do
	SSID=`echo $pair|cut -f1 -d:`
	PSK=`echo $pair|cut -f2 -d:`
	wpa_passphrase $SSID $PSK >> /etc/wpa_supplicant/wpa_supplicant.conf
done
#wpa_passphrase `get_param wlan_ssid` `get_param wlan_psk` >> /etc/wpa_supplicant/wpa_supplicant.conf


IP=`get_param lan_addr`
O1=`echo $IP | cut -f1 -d.`
O2=`echo $IP | cut -f2 -d.`
O3=`echo $IP | cut -f3 -d.`
O4=`echo $IP | cut -f4 -d.`

(
cat <<DHCPD
interface `get_param wlan_if`
static ip_address=$IP/24
static routers=$O1.$O2.$O3.0
DHCPD
) > /etc/dhcpd.conf


# hostapd only needed if routing from one wlan to another, i.e. wifi bridging, leaving it for now
# https://pimylifeup.com/raspberry-pi-wifi-extender
mkdir -p /etc/hostapd
(
cat <<HOSTAPD

HOSTAPD
) > /etc/hostapd/hostapd.conf


DHCP_RANGE=`get_param lan_dhcp_range`
DHCP_RANGE2=`echo $DHCP_RANGE|cut -f1 -d\ `,`echo $DHCP_RANGE|cut -f2 -d\ `,12h
(
cat <<DNSMASQ
interface=$(get_param lan_if)
listen-address=$(get_param lan_addr)
bind-interfaces
server=8.8.8.8
domain-needed
bogus-priv
dhcp-range=$DHCP_RANGE2
DNSMASQ
) > /etc/dnsmasq.d/local.conf

#set -x
#grep "^net.ipv4.ip_forward" /etc/sysctl.conf
#ERR=$?
#echo $ERR
#if [ $ERR = 0 ]
#then
#	sed -i "s/^net.ipv4.ip_forward.*$/net.ipv4.ip_forward=1/" /etc/sysctl.conf
#
#else
#	echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
#fi

echo 1 > /proc/sys/net/ipv4/ip_forward

WLAN_IF=`get_param wlan_if`
LAN_IF=`get_param lan_if`

(
cat <<FORWARDBIN

echo 1 > /proc/sys/net/ipv4/ip_forward
/sbin/iptables --flush
/sbin/iptables -t nat -A POSTROUTING -o $WLAN_IF -j MASQUERADE
/sbin/iptables -A FORWARD -i $LAN_IF -o $WLAN_IF -m state --state RELATED,ESTABLISHED -j ACCEPT
/sbin/iptables -A FORWARD -i $WLAN_IF -o $LAN_IF -j ACCEPT
exit 0

FORWARDBIN
)>  /usr/bin/forwarding-iptables
chmod +x /usr/bin/forwarding-iptables

(
cat <<RCLOCAL
#!/bin/bash

# https://stackoverflow.com/a/13722274
exec 2> /tmp/rc.local.log  # send stderr from rc.local to a log file
exec 1>&2                      # send stdout to the same log file
set -x                         # tell sh to display commands before execution



ifconfig $WLAN_IF down
sleep 1
iw reg set GY
ifconfig $WLAN_IF up
iwconfig $WLAN_IF channel 13
iwconfig $WLAN_IF txpower 30
sleep 2


/usr/bin/forwarding-iptables
date > /home/pi/rc.local-ran
exit 0
RCLOCAL
)> /etc/rc.local
chmod +x /etc/rc.local


(
cat <<SYSTEMD
[Unit]
Description = set up forwarding iptables rules
After = network.target
[Service]
ExecStart = /usr/bin/forwarding-iptables
[Install]
WantedBy = multi-user.target

SYSTEMD
)> /etc/systemd/system/ipforward.service

systemctl daemon-reload
systemctl enable ipforward
systemctl start ipforward


if [ 1 = 0 ]
then
apt-get update
apt-get -y upgrade
apt-get -y install dnsmasq hostapd
fi
#systemctl stop hostapd
#systemctl stop dnsmasq

systemctl enable dnsmasq
systemctl enable hostapd
systemctl disable wpa_supplicant

systemctl restart dnsmasq
systemctl restart hostapd
systemctl stop wpa_supplicant

#/usr/bin/forwarding-iptables
