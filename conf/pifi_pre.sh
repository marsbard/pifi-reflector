BANNER="
        ${YELLOW}=================================\n\
	${WHITE}pifi-reflector - Pi Wifi Extender\n\
	${YELLOW}=================================${RESET}"


pushd "$(dirname $0)"/../conf

# find interfaces, skip loopback
for IF in $(ip a|grep "^.:"|cut -f2 -d\ |sed "s/://")
do
	if [ "$IF" = "lo" ]
	then
		continue
	fi
	IFs="$IFs|$IF"
done

# strip first pipe
IFs=$(echo $IFs|cut -c2-)


# write start of parameters file
(
cat <<EOF
IDX=0
params[\$IDX]="wlan_if"
descr[\$IDX]="Interface to use for upstream WLAN connnection"
required[\$IDX]=1
choices[\$IDX]="$IFs"

IDX=\$(( \$IDX + 1 ))
params[\$IDX]="lan_if"
descr[\$IDX]="Interface to use for downstream LAN connnection"
required[\$IDX]=1
choices[\$IDX]="$IFs"

EOF
) > pifi_params.sh 

cat pifi_params.sh.stub >> pifi_params.sh

chmod +x pifi_params.sh


popd
